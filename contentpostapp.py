import webapp

formulario_POST = '<form action="" method="post" class="form-example" ' \
                + '<div class="form-example">' + '<label for="recurso">Introduce un recurso aqui: ' \
                + '</label>' + '<input type="text" name="recurso" id="recurso" required>' \
                + '</div>' + '<div class="form-example">' + '<input type="submit" value="Enviar!">' \
                + '</div></form>'


class contentAPP(webapp.webapp):

    recursos = {'/':'p&aacute;gina principal',
               '/sara':'p&aacute;gina de sara'}

    def parse(self, received):
        recibido = received.decode()
        metodo = recibido.split(' ')[0]
        recurso = recibido.split(' ')[1]
        if metodo == "POST":
            body = recibido.split('\r\n\r\n')[1]
        else:
            body = None

        return metodo, recurso, body  #Tupla, solo devuelvo un valor

    def process(self, analyzed):

        metodo, recurso, body = analyzed

        if metodo == "POST":
            self.recursos[recurso] = body
            http = "200 OK"
            html = "<html><body><h4>Introducir contenido</h4>El recurso solicitado es: " \
                    + recurso + "<br>El contenido del recurso es: " + self.recursos[recurso] \
                    + formulario_POST + "</body></html>"
        elif metodo == "PUT":
            self.recursos[recurso] = body
            http = "200 OK"
            html = "<html><body>Content added successfully</body></html>"
        elif recurso in self.recursos.keys():
            http = "200 OK"
            html = "<html><body><h4>P&aacute;gina encontrada</h4>El recurso pedido es: " \
                   + recurso + "<br>" + "El contenido pedido es: " + self.recursos[recurso] \
                   + "<p>Introduce un nuevo recurso: </p>" + formulario_POST + "</body></html>"
        else:
            http = "404 NOT FOUND"
            html = "<html><body><img src='https://upload.wikimedia.org/wikipedia/commons/9/9b/404-error-css.png'/></body></html>"
        return http, html


if __name__ == "__main__":
    contentAPP= contentAPP('localhost', 1234)
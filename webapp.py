import socket


class webapp:

    #Le paso lo que he recibido
    def parse(self, received):
        recibido = received.decode()
        return recibido.split(' ')[1]

    #Lo que ya he analizado
    def process(self, analyzed): #En analyzed se quedará el valor cogido en split de la función parse
        http = "200 OK" #Respuesta http
        html = "<html<body><h1>Hello World! Tu peticion es " + analyzed + "</h1>" \
                + "</body></html>"#Respuesta html

        return http, html

    def __init__(self, ip, port):
        mySocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        mySocket.bind((ip, port))

        mySocket.listen(5)

        while True:
            print("Waiting for connection...")
            (recvSocket, address) = mySocket.accept()
            print(address)
            print("HTTP request received")
            received = recvSocket.recv(2048)
            print(received)

            petition = self.parse(received) #Trato lo que he recibido
            http, html = self.process(petition) #Dos variables --> vvalo http porque vamos a tener variables que cambiarán de valor

            response = "HTTP/1.1" + http + "\r\n\r\n" \
            + html + "\r\n"

            recvSocket.send(response.encode('utf-8'))
            recvSocket.close()


if __name__ == "__main__":
    webapp = webapp('localhost', 1234)
